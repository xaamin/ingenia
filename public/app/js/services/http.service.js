(function () {
    var app = angular.module('eculab');

    app.service('Loading', ['$rootScope', '$mdDialog', function ($rootScope, $mdDialog) {
        var service = {};

        service.show = _show;
        service.hide = _hide;

        return service;

        function _show() {
            $mdDialog.show({
                controller: DialogController,
                template: '<md-dialog id="plz_wait" style="background-color:transparent;box-shadow:none">'
                            + '<div layout="row" layout-sm="column" layout-align="center center" aria-label="wait">'
                            +    '<md-progress-circular md-mode="indeterminate" md-diameter="70"></md-progress-circular>'
                            + '</div>'
                        + '</md-dialog>',
                parent: angular.element(document.body),
                clickOutsideToClose:false,
                fullscreen: false
            })
            .then(function(answer) {
                
            });
        }

        function _hide() {
            $rootScope.$emit("hide_wait");
            
            $mdDialog.cancel();
        }

        function DialogController($scope, $mdDialog) {
        }
    }]);
    
    app.service('HttpService', ['$http', '$q', 'Loading', '$timeout', function($http, $q, Loading, $timeout) {        
        var service = {};
        var baseUrl = '';
        var showLoading = true;
        var timeout;
        var result;
        var timedOut = false;
        var timedOutVar;

        service.get = _get;
        service.store = service.post = _store;
        service.update = service.put = _update;
        service.delete = _delete;
        service.setBaseUrl = _setBaseUrl;
        service.bypass = _bypass;

        return service;

        /**
         * Public API
         */
            
        function _get(url, params) {
            _bootstrap();
            
            var request = $http({
                method: "get",
                url: _buildUrl(url),
                params: params,                
                timeout: timeout.promise
            });

            return _promise(request);
        }

        function _store(url, data) {
            _bootstrap();
            
            var request = $http({
                method: "post",
                url: _buildUrl(url),
                data: data,
                timeout: timeout.promise
            });

            return _promise(request);
        }

        function _update(url, data) {
            _bootstrap();
            
            var request = $http({
                method: "put",
                url: _buildUrl(url),
                data: data,
                timeout: timeout.promise
            });

            return _promise(request);
        }

        function _delete(url) {
            _bootstrap();
            
            var request = $http({
                method: "delete",
                url: _buildUrl(url)
            });

            return _promise(request);
        }

        function _promise(request) {
            if (showLoading) {
                Loading.show();
            }

            return request.then(_success, _error);
        }

        function _success(response) {
            setTimeout(function () {
                Loading.hide();
            });

            showLoading = false;

            if (timedOutVar) {
                clearTimeout(timedOutVar);
            }

            return response.data;
        }

        function _error(response) {
            Loading.hide();
            showLoading = false;

            if (timedOutVar) {
                clearTimeout(timedOutVar);
            }

            if (timedOut) {
                return $q.reject({
                    success: false,
                    message: 'Request took longer than 60 seconds.'
                });
            }
            
            if (angular.isObject(response.data)) {
                return $q.reject(response.data);
            }

            return $q.reject("An unknown error occurred. [" + (response.status || 'No code') + ". " + (response.statusText || 'No description') + "]");
        }

        function _buildUrl(url) {
            if (!/^https?:\/\//i.test(url)) {
                url = baseUrl.replace(/\/+$/g, '') + '/' + url.replace(/^\/+|\/+$/g, '');
            }

            return url;
        }

        function _setBaseUrl(url) {
            baseUrl = url;
        }

        function _bypass() {            
            showLoading = false;
        }

        function _bootstrap() {
            timeout = $q.defer();
            result = $q.defer();
            timedOut = false;
            
            timedOutVar = setTimeout(function () {
                timedOut = true;
                timeout.resolve();
            }, (1000 * 60));
        }
    }]);
})();
