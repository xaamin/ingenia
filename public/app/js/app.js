(function () {
	var app = angular.module( 'eculab', [ 'ngMaterial', 'ui.router', 'ngMdIcons', 'md.data.table', 'ngMessages' ] );

    app.config(function($stateProvider, $urlRouterProvider) {
		// For any unmatched url, redirect to /state1
		$urlRouterProvider.otherwise("/state1");
		
		// Now set up the states
		$stateProvider.state('app', {
	        url: '/',
	        abstract: true,
	        templateUrl: 'app/templates/app.html',
	        controller: 'AppController as app'
	    })
	    .state('app.empleados', {
            url: 'empleados',
            views: {
            	content: {
                    templateUrl: 'app/templates/empleados/new.html',
            		controller: 'EmpleadoController as vm',
                }
            },
            resolve: {
                Employees: function () {
                    return false;
                }
            }
        })
        .state('app.empleados.list', {
            url: '/list',
            views: {
            	'content@app': {
                    templateUrl: 'app/templates/empleados/list.html',
                    controller: 'EmpleadoController as vm',
                }
            },
            resolve: {
                Employees: function (HttpService) {
                    return HttpService.get('/empleado', function (response) {
                        return response.results;
                    }, function () {
                        return [];
                    });
                }
            }
        });

		// if none of the above states are matched, use this as the fallback
       	$urlRouterProvider.otherwise('/empleados');
	});
})();