(function () {
	var app = angular.module('eculab');

    app.controller("AppController", function ($rootScope, $mdDialog, $mdSidenav) {
    	console.log('App controller loaded');

    	var vm = this;
    	
    	$rootScope.$on("hide_wait", function (event, args) {
            $mdDialog.cancel();
        });

        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){ 
        	$mdSidenav('left')
	          	.close()
	   	    	.then(function () {
	           		console.log('Close sidenav left is done');
	       		});
        });

    	vm.toggle = function () {
    	 	$mdSidenav('left')
	          	.toggle()
	          	.then(function () {
	            	console.log('Toogle sidenav left is done');
	        	});
    	}
    });
})();