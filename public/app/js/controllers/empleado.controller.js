(function () {
	var app = angular.module('eculab');

	app.controller("EmpleadoController", function ($rootScope, $scope, $mdDialog, HttpService, $state, Employees, $timeout) {
    	console.log('Empleado controller loaded');

    	var vm = this;

    	vm.employee = {};
    	vm.selected = [];
    	vm.employees = Employees ? Employees.results.data : [];
    	console.log(Employees);

    	vm.pagination = {};

    	vm.limits = [5, 10, 15, 20, 25];

    	vm.labels = {
    		page: 'Página',
    		rowsPerPage: 'Resultados por página',
    		of: 'de'
    	};

    	updatePagination(Employees);

		vm.cancelRegistration = function(ev, form) {
	    	console.log(form);  

	    	var confirm = $mdDialog.confirm()
		        .title('Cancelar captura?')
		        .textContent("Los datos capturados se perderán. Continuar?")
		        .ariaLabel('Confirmación de cancelación')
		        .targetEvent(ev)
		        .ok('Sí, continuar')
		        .cancel('Lo pensaré');

	    	$mdDialog.show(confirm).then(function() {
	      		vm.employee = {};	      		

		  		form.$setValidity();
		        form.$setPristine();
		        form.$setUntouched();
	    	}, function() {
	      		// Ha decidido no cancelar la captura
	    	});
	  	};

	  	vm.filterByID = function (id) {
	  		vm.paginate(1, vm.pagination.per_page, id);
	  		vm.search = '';
	  	}

	  	vm.submitEmployee = function (employee, form, ev) {
	  		console.log(form);  
	  		HttpService.store('/empleado', employee).then(function (response) {
	  			vm.employee = {};
	  			console.log('Response', response);
	  			$timeout(function () {
	  				$mdDialog.show(
					    $mdDialog.alert()
					        .clickOutsideToClose(true)
					        .title('Notificación')
					        .textContent(response.message)
					        .ariaLabel('Response content')
					        .ok('Ok!')
					        .targetEvent(ev)
					);

					form.$setValidity();
		        	form.$setPristine();
		        	form.$setUntouched();
	  			}, 300);
	  			//$state.go('app.presupuestos.list');
	  		}, function (error) {
	  			alert(error);
	  			console.log('Error', error);
	  		});
	  	};

	  	vm.paginate = function (page, perPage, id ) {
	  		console.log(page, perPage)
	  		vm.promise = HttpService.get('/empleado?page=' + (page || 1) + '&limit=' + perPage + '&id=' + (id || '')).then(function (response) {
                vm.employees = response.results ? response.results.data : [];
                updatePagination(response);
            }, function () {
                vm.employee = [];
            }).$promise;
	  	};

	  	function updatePagination(pagination) {
	  		vm.pagination =  {
	    		current_page: pagination ? pagination.results.current_page : 1,
	    		per_page: pagination ? parseInt(pagination.results.per_page) : 5,
	    		total: pagination ? pagination.results.total : 0
	    	}
	  	}
    });

	function DialogController($scope, $mdDialog, $log, HttpService, $q, $http) {
		var vm = this;

		vm.suggestions = [];

	  	vm.hide = function () {
	    	$mdDialog.hide();
	  	};

	  	vm.cancel = function () {
	    	$mdDialog.cancel();
	  	};

	  	vm.answer = function (product) {
	    	$mdDialog.hide(product);
	  	};
	}
})();