<?php
return [
	
	'interiores' => [
		'espejo_retrovisor' => 'Espejo retrovisor',
		'encendedor' => 'Encendedor',
		'ceniceros' => 'Ceniceros',
		'botones' => 'Botones',
		'radio' => 'Radio',
		'viceras' => 'Viceras',
		'llaveros' => 'Llavero',
		'cinturones' => 'Cinturones',
		'manijas' => 'Manijas',
		'tapetes_alfombra' => 'Tapetes alfombra',
		'tapetes_plastico' => 'Tapetes plástico',
		'cabeceras' => 'Cabeceras',
		'tarjeta_circulacion' => 'Tarj. circulación',
		'comprobante_verificacion' => 'Comp. verificación',
		'testitos_encendidos' => 'Testigos encendidos',
		'claxon' => 'Claxon',
		'birlo_seguridad' => 'Birlo seguridad'
	], 

	'exteriores' => [
		'faros' => 'Faros',
		'cuartos' => 'Cuartos',
		'espejos_y_tapas' => 'Espejos y tapas',
		'calaveras' => 'Calaveras',
		'emblemas' => 'Emblemas',
		'tapones_rueda' => 'Tapones rueda (4)',
		'molduras' => 'Molduras',
		'limpiadores' => 'Limpiadores (Plumas)',
		'tapon_de_gasolina' => 'Tapón de gasolina',
		'placas' => 'Placas',
		'cristales_rotos' => 'Cristales rotos',
		'golpes' => 'Golpes',
		'pintura_rayada' => 'Pintura rayada'
	],

	'cajuela' => [
		'gato' => 'Gato',
		'llave_de_tuercas' => 'Llave de tuercas',
		'herramientas' => 'Herramienta',
		'reflejantes' => 'Reflejantes',
		'extinguidor' => 'Extinguidor',
		'cables_corriente' => 'Cables corriente',
		'llanta_de_refaccion' => 'Llanta refacción'
	],

	'mecanica' => [
		'tapon_de_aceite' => 'Tapón de aceite',
		'tapon_radiador' => 'Tapón radiador',
		'varilla_de_aceite' => 'Varilla de aceite',
		'portafiltro_aire' => 'Portafiltro aire',
		'bateria' => 'Bateria'
	]
];