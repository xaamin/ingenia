# Prueba de evaluación técnica para el área de desarrollo de Pago fácil.

### Objetivo:
Crear una aplicación web en php que permita registrar empleados con las siguientes
características.
Datos de los empleados:
­Nombre
­Apellido Paterno
­Apellido Materno
­Fecha de nacimiento (El registro de la fecha será en formato dd/mm/Y)
­Ingresos anuales (cantidad a 2 decimales)
Para esta prueba puede utilizar el utilizar el framework de PHP de su preferencia.
Puede hacer uso de las librerías de php/javascript que desee.

### Tiempo estimado:
4 a 6 hrs

### Instrucciones:

1. Crear 2 tablas una que contenga el nombre del usuario y la segunda tabla que tenga la fecha
de nacimiento y los ingresos anuales.
Datos para la conexión:
Base de datos examen
Usuario: examen
Pwd: examen
2. Crear una pagina la cual contenga un formulario que permita ingresar nuevos empleados.
3. Crear una pagina la cual muestre el listado de los empleados guardados y crear un
formulario que permita consultar un empleado por medio de el ID del empleado.
4. Crear una API por medio de un servicio web REST que permite agregar empleados y
consultar un empleado por medio del envío del ID del empleado desde el consumo de la
misma.

### Puntos Extras

1. Validación de los datos que se ingresan en el formulario.
2. Utilizar ajax para guardar o mostrar los datos.
3. Utilizar Bootstrap (la versión que desee) para la interface gráfica de las paginas
