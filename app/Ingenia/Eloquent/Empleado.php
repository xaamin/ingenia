<?php
namespace App\Ingenia\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
	protected $table = 'empleados';

	protected $fillable = ['nombre', 'apellido_paterno', 'apellido_materno'];

	public function detalles()
	{
		return $this->hasOne('App\Ingenia\Eloquent\EmpleadoInformacion');
	}
}