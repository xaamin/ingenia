<?php
namespace App\Ingenia\Eloquent;

use Illuminate\Database\Eloquent\Model;

class EmpleadoInformacion extends Model
{
	protected $table = 'empleados_informacion';

	protected $fillable = ['fecha_de_nacimiento', 'ingresos_anuales'];
}