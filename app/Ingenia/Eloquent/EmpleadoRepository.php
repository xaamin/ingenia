<?php
namespace App\Ingenia\Eloquent;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Ingenia\Contracts\EmpleadoRepositoryInterface as RepositoryContract;

class EmpleadoRepository implements RepositoryContract
{
	protected $request;

	protected $empleado;

	/**
	 * Constructor
	 * @param \Illuminate\Http\Request  			$request  
	 * @param \Illuminate\Database\Eloquent\Model 	$empleado 
	 */
	public function __construct(Request $request, Empleado $empleado)
	{
		$this->request = $request;
		$this->empleado = $empleado;
	}

	public function save(array $data)
	{
		$empleado = $this->empleado->fill($data);
		$empleado->save();

		$information = $this->request->only(['fecha_de_nacimiento', 'ingresos_anuales']);
		$information['fecha_de_nacimiento'] = Carbon::createFromFormat('d/m/Y', $information['fecha_de_nacimiento']);

		$empleado->detalles()->create($information);

		return $empleado;
	}

	public function paginate($perPage, array $projection = null)
	{
		$empleado = $this->empleado->with('detalles');

		$id = $this->request->input('id');

		if ($id) {
			$empleado->where('id', '=', $id);
		}

		return $empleado->paginate($perPage);
	}
}