<?php
namespace App\Ingenia\Contracts;

use Illuminate\Database\Eloquent\Model;

interface EmpleadoRepositoryInterface
{
	/**
	 * Persists employee to database
	 * 
	 * @param  array $data 
	 * @return bool
	 */
	public function save(array $data);

	/**
	 * Paginates results 
	 * 
	 * @param  integer $perPage    
	 * @param  array $projection Fields
	 * @return \Illuminate\Support\Collection
	 */
	public function paginate($perPage, array $projection = null);
}