<?php
namespace App\Providers;

use Xaamin\PDF\PDF;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->setPDFSharedInstance();
    }

    protected function setPDFSharedInstance()
    {
        $this->app->bind('Xaamin\PDF\PDF', function ($app) {
            return new PDF('P', 'mm', 'letter');
        });
    }
}
