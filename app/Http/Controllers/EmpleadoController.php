<?php
namespace App\Http\Controllers;

use DB;
use Cache;
use Illuminate\Http\Request;
use App\Ingenia\Eloquent\EmpleadoRepository as Empleado;

class EmpleadoController extends Controller
{
	public function index()
	{
		return view('layout.material');
	}

	public function lists(Request $request, Empleado $empleado)
	{		
		$data = [
			'success' => true,
			'status' => 'success',
			'message' => 'Empleados registrados',
			'results' => $empleado->paginate($request->input('limit', 5))
		];
		
		return response()->json($data);
	}

	public function save(Request $request, Empleado $empleado)
	{
		$data = [			
			'success' => true,
			'status' => 'success',
			'message' => 'Empleado registrado',
		];

		$empleado->save($request->all());

		return response()->json($data);
	}
}