<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadosInformacionTable extends Migration
{
   /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados_informacion', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empleado_id');
            $table->date('fecha_de_nacimiento');
            $table->decimal('ingresos_anuales');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('empleados_informacion');
    }
}
