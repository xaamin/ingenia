<?php
use App\Ingenia\Eloquent\Empleado;
use Illuminate\Database\Seeder;

class EmpleadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $empleados = [
            [
                'personal' => [
                    'nombre' => 'Benjamín',
                    'apellido_paterno' => 'Martínez',
                    'apellido_materno' => 'Mateos'
                ],
                'adicional' => [
                    'fecha_de_nacimiento' => '1985-06-12',
                    'ingresos_anuales' => 20000
                ]
            ],
            [
                'personal' => [
                    'nombre' => 'America',
                    'apellido_paterno' => 'Rosas',
                    'apellido_materno' => 'Mendoza'
                ],
                'adicional' => [
                    'fecha_de_nacimiento' => '1988-08-02',
                    'ingresos_anuales' => 50000
                ]
            ],
            [
                'personal' => [
                    'nombre' => 'Mayra',
                    'apellido_paterno' => 'Baños',
                    'apellido_materno' => 'Hernández'
                ],
                'adicional' => [
                    'fecha_de_nacimiento' => '1988-07-13',
                    'ingresos_anuales' => 15000
                ]
            ],
            [
                'personal' => [
                    'nombre' => 'David',
                    'apellido_paterno' => 'Martínez',
                    'apellido_materno' => 'Barragán'
                ],
                'adicional' => [
                    'fecha_de_nacimiento' => '1989-10-18',
                    'ingresos_anuales' => 60000
                ]
            ],
            [
                'personal' => [
                    'nombre' => 'Lucía Anahí',
                    'apellido_paterno' => 'Valencia',
                    'apellido_materno' => 'Mendoza'
                ],
                'adicional' => [
                    'fecha_de_nacimiento' => '2000-12-03',
                    'ingresos_anuales' => 15000
                ]
            ],
            [
                'personal' => [
                    'nombre' => 'Ivonne',
                    'apellido_paterno' => 'Guevara',
                    'apellido_materno' => 'Castillo'
                ],
                'adicional' => [
                    'fecha_de_nacimiento' => '2002-04-23',
                    'ingresos_anuales' => 55000
                ]
            ]
        ];

        foreach ($empleados as $data) {
            $empleado = new Empleado($data['personal']);
            $empleado->save();

            $empleado->detalles()->create($data['adicional']);
        }
    }
}
